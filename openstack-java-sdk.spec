# % global git_hash git10597f7

Name:           openstack-java-sdk
Version:        3.2.9
Release:        1
Summary:        OpenStack Java SDK

License:        Apache-2.0
URL:            https://github.com/woorea/%{name}
Source0:        https://github.com/woorea/%{name}/archive/%{name}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  jpackage-utils >= 0:1.7.3
BuildRequires:  jackson-annotations >= 2.9.0
BuildRequires:  jackson-core >= 2.9.0
BuildRequires:  jackson-databind >= 2.9.0
BuildRequires:  jackson-jaxrs-json-provider >= 2.9.0
BuildRequires:  maven-local
BuildRequires:  sonatype-oss-parent
BuildRequires:  mvn(org.apache.httpcomponents:httpclient) >= 4.5.0
BuildRequires:  mvn(org.jboss.resteasy:resteasy-jaxrs)
BuildRequires: mvn(javax.xml.bind:jaxb-api) >= 2.2.12

Requires:  jackson-annotations >= 2.9.0
Requires:  jackson-core >= 2.9.0
Requires:  jackson-databind >= 2.9.0
Requires:  jackson-jaxrs-json-provider >= 2.9.0

%description
OpenStack client implementation in Java.

%package -n openstack-java-javadoc
Summary:        Java docs for OpenStack Java SDK

%description -n openstack-java-javadoc
This package contains the API documentation for the OpenStack Java SDK.

%package -n openstack-java-client
Summary:        OpenStack Java Client

%description -n openstack-java-client
This package contains the %{summary}.

%package -n openstack-java-resteasy-connector
Summary:        OpenStack Java RESTEasy Connector

%description -n openstack-java-resteasy-connector
This package contains the %{summary}.
Requires:  mvn(org.apache.httpcomponents:httpclient) >= 4.5.0

%package -n openstack-java-ceilometer-client
Summary:        OpenStack Java Ceilometer Client

%description -n openstack-java-ceilometer-client
This package contains the %{summary}.

%package -n openstack-java-ceilometer-model
Summary:        OpenStack Java Ceilometer Model

%description -n openstack-java-ceilometer-model
This package contains the %{summary}.

%package -n openstack-java-glance-client
Summary:        OpenStack Java Glance Client

%description -n openstack-java-glance-client
This package contains the %{summary}.

%package -n openstack-java-glance-model
Summary:        OpenStack Java Glance Model

%description -n openstack-java-glance-model
This package contains the %{summary}.

%package -n openstack-java-cinder-client
Summary:        OpenStack Java Cinder Client

%description -n openstack-java-cinder-client
This package contains the %{summary}.

%package -n openstack-java-cinder-model
Summary:        OpenStack Java Cinder Model

%description -n openstack-java-cinder-model
This package contains the %{summary}.

%package -n openstack-java-keystone-client
Summary:        OpenStack Java Keystone Client

%description -n openstack-java-keystone-client
This package contains the %{summary}.

%package -n openstack-java-keystone-model
Summary:        OpenStack Java Keystone Model

%description -n openstack-java-keystone-model
This package contains the %{summary}.

%package -n openstack-java-nova-client
Summary:        OpenStack Java Nova Client

%description -n openstack-java-nova-client
This package contains the %{summary}.

%package -n openstack-java-nova-model
Summary:        OpenStack Java Nova Model

%description -n openstack-java-nova-model
This package contains the %{summary}.

%package -n openstack-java-quantum-client
Summary:        OpenStack Java Quantum Client

%description -n openstack-java-quantum-client
This package contains the %{summary}.

%package -n openstack-java-quantum-model
Summary:        OpenStack Java Quantum Model

%description -n openstack-java-quantum-model
This package contains the %{summary}.

%package -n openstack-java-swift-client
Summary:        OpenStack Java Swift Client

%description -n openstack-java-swift-client
This package contains the %{summary}.

%package -n openstack-java-swift-model
Summary:        OpenStack Java Swift Model

%description -n openstack-java-swift-model
This package contains the %{summary}.

%package -n openstack-java-heat-client
Summary:        OpenStack Java Heat Client

%description -n openstack-java-heat-client
This package contains the %{summary}.

%package -n openstack-java-heat-model
Summary:        OpenStack Java Heat Model

%description -n openstack-java-heat-model
This package contains the %{summary}.


%prep
%setup -q -n %{name}-%{name}-%{version}
%mvn_package ":{openstack-java-sdk,openstack-client-connectors}" __noinstall

%build
%mvn_build -s -b -- -P "!console,!examples,!jersey2,!jersey,resteasy" -DskipTests

%install
%mvn_install


%files -n openstack-java-javadoc -f .mfiles-javadoc
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-client -f .mfiles-openstack-client
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-resteasy-connector -f .mfiles-resteasy-connector
%doc LICENSE.txt README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-ceilometer-client -f .mfiles-ceilometer-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-ceilometer-model -f .mfiles-ceilometer-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-glance-client -f .mfiles-glance-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-glance-model -f .mfiles-glance-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-cinder-client -f .mfiles-cinder-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-cinder-model -f .mfiles-cinder-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-keystone-client -f .mfiles-keystone-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-keystone-model -f .mfiles-keystone-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-nova-client -f .mfiles-nova-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-nova-model -f .mfiles-nova-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-quantum-client -f .mfiles-quantum-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-quantum-model -f .mfiles-quantum-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-swift-client -f .mfiles-swift-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-swift-model -f .mfiles-swift-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%files -n openstack-java-heat-client -f .mfiles-heat-client
%license LICENSE.txt
%doc README.textile

%files -n openstack-java-heat-model -f .mfiles-heat-model
%license LICENSE.txt
%doc README.textile
%dir %{_javadir}/%{name}

%changelog
* Tue Aug 17 2021 Python_Bot <Python_Bot@openeuler.org> - 3.2.9-1
- Init Package
